#!/usr/bin/env python3

import concurrent.futures
import os
import requests
import sys
import time

# Functions

def usage(status=0):
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [-h HAMMERS -t THROWS] URL
    -h  HAMMERS     Number of hammers to utilize (1)
    -t  THROWS      Number of throws per hammer  (1)
    -v              Display verbose output
    ''')
    sys.exit(status)

def hammer(url, throws, verbose, hid):
    ''' Hammer specified url by making multiple throws (ie. HTTP requests).

    - url:      URL to request
    - throws:   How many times to make the request
    - verbose:  Whether or not to display the text of the response
    - hid:      Unique hammer identifier

    Return the average elapsed time of all the throws.
    '''

    total_time = 0

    for i in range(throws):
        start_time = time.time()

        response = requests.get(url)
        
        if(verbose):
            print(response.text)

        elapsed_time = time.time() - start_time
        total_time += elapsed_time

        print(f"Hammer: {hid}, Throw:   {i}, Elapsed Time: {elapsed_time:.2f}")

    
    average_time = total_time / throws
    print(f"Hammer: {hid}, AVERAGE   , Elapsed Time: {average_time:.2f}")
    

    return average_time

def do_hammer(args):
    ''' Use args tuple to call `hammer` '''
    return hammer(*args)

def main():
    hammers = 1
    throws  = 1
    verbose = False

    # Parse command line arguments
    arguments = sys.argv[1:]
    while arguments and arguments[0].startswith('-'):
        argument = arguments.pop(0)
        if argument == '-h':
            hammers = int(arguments.pop(0))
        elif argument == '-t':
            throws = int(arguments.pop(0))
        elif argument == '-v':
            verbose = True
        else:
            usage(1)

    if arguments:
        url = arguments[0]
    else:
        usage(1)

    avg_time = hammer(url, throws, verbose, 0) 
    print(avg_time)

    # Create pool of workers and perform throws
    

# Main execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
